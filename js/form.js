$(document).ready(function() {
    var gumb = $("#submit");
    gumb.on("click", submit);
    /*var addNew = $("#add-new-member");
    addNew.on("click", addNewMember);*/
});

/*
function addNewMember(){
    var members = $(".member-input");
    var newMember = $(members[members.length - 1]).clone();
    var oldId = parseInt(newMember[0].id.split("-")[1]);
    var newId = oldId + 1;
    if (newId === 4){
        $("#add-new-member").css("display", "none");
    }
    newMember[0].id = "member-" + newId;
    $(newMember.children()[0]).text($(newMember.children()[0]).text().split("#")[0] + "#" + newId);
    $.each(newMember.find("input"), function(i, element){
        id = element.id.split("-");
        id[1] = newId;
        element.id = id.join("-");
    });
    newMember.css("display", "none");
    newMember.appendTo("form");
    newMember.toggle(250);
}*/

function submit(){
    var team;
    var password;
    var members;
    team = $("#teamname")[0].value;
    password = $("#password")[0].value;
    members = [];

    var mems = $(".member-input");
    var newMember = $(mems[mems.length - 1]);
    numberOfMembers = parseInt(newMember[0].id.split("-")[1]);

    for (var i = 1; i <= numberOfMembers; i++){
        var temp = {};
        temp["name"] = $("#member-" + i + "-name")[0].value;
        temp["surname"] = $("#member-" + i + "-surname")[0].value;
        temp["mail"] = $("#member-" + i + "-email")[0].value;
        members.push(temp);
    }

    data = {
        teamname : team,
        password : password,
        members : members
    };

    $.ajax({
        url: 'http://52.233.158.172/change/api/hr/account/register',
        type: 'POST',
        contentType: 'application/json',
        data : JSON.stringify(data),
        success: function(response){
            window.location.href = 'login.html';
        },
        error : function(response){
            var errors = (response["responseJSON"]["Errors"]);
            $.each(errors, function(i, error){
                if (error.includes("Mail") || error.includes("Team name")){
                    $(".alert-danger").css("display", "inherit");
                }
                if (error.includes("Name") || error.includes("Surname")){
                    $(".alert-warning").css("display", "inherit");   
                }
            });
        }
    });
}
