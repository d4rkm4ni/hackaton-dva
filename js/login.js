$(document).ready(function(){
	$("#submit").on("click", login);
    $("#register").on("click", gotoRegister);
});


function login(){
	teamname = $("#teamname")[0].value;
	password = $("#password")[0].value;
	data = {
		teamname : teamname,
		password : password
	};
	$.ajax({
		url: "http://52.233.158.172/change/api/hr/account/login",
		type: "POST",
		contentType: 'application/json',
        data: JSON.stringify(data),
		success: function(response) {
			var data = JSON.parse(response['Result']);
            var teamId = data['TeamId'];
            var authToken = data['AuthorizationToken'];
			gotoMembers(teamId);
		},
		error: function(response) {
			var errorMessages = response['responseJSON']['Errors'];
            openErrorAlert(errorMessages[0]);
		}
	});
}

function gotoMembers(teamId, authToken) {
	localStorage.setItem('teamId', teamId);
    localStorage.setItem('authToken', authToken);
	window.location.href = 'members.html';
}

function openErrorAlert(errorMessage) {
	$(".alert").remove();
	var alert = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"display:block\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\">×</a>" + errorMessage +"</div>";
	$("#submit").before(alert);
}

function gotoRegister() {
    window.location.href = 'register.html';
}