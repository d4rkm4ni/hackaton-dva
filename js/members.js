
// A default team id (d4rkm4ni) in case team id is not specified
const FALLBACK_TEAM_ID = 16;
const FALLBACK_AUTH_TOKEN = 'ZDRya200bmk6Og==';

// Get members when document loads
$( document ).ready(getMembers);

// A function that GETs team members from the server
function getMembers() {
    var teamId = localStorage.getItem('teamId');
    var authToken = localStorage.getItem('authToken');
    if(teamId === null) {
        teamId = FALLBACK_TEAM_ID;
        authToken = FALLBACK_AUTH_TOKEN;
    }

	$.ajax({
        type: 'GET',
        url: 'http://52.233.158.172/change/api/en/team/details/' + teamId,
        contentType: 'application/json',
        headers: {
            'X-Authorization': authToken
        },
        success: function(response) {
            var data = JSON.parse(response['Result']);
            appendMembers(data['TeamName'], data['Members']);
        }
    });
}

// A function that appends member data to the member list
function appendMembers(teamName, memberData) {
    var title = $('#title');
    title.text(teamName);
    title.css('opacity', 0);
    title.animate({
        'opacity' : '1'
    }, 600);

    for(var i = 0; i < memberData.length; i++) {
		var item = $('<li>');
		
		var name = $('<label>');
		name.text(memberData[i].Name + " " + memberData[i].Surname);
		item.append(name);
		
		var mail = $('<label>');
		mail.append(memberData[i].Mail);
		item.append(mail);
		
		$('#member_list').append(item);

		item.css('opacity', 0);
        item.css('margin-right', '30px');

		item.animate({
            'opacity': '0'
        }, i * 300 + 300);

		item.animate({
            'opacity': '1',
            'margin-right': '0px'
        }, 400);
	}
}